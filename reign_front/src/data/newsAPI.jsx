import { baseurl } from '../utils/global_vars';
import axios from 'axios';

const httpRequest = axios.create({
  baseURL: baseurl,
});

export const getNewsAPI = async () => {
  try {
    const response = await httpRequest.get(`news`).then((response) => response);

    if (response.status === 200) {
      return response.data;
    }
    throw Error(response);
  } catch (error) {
    console.log('There was an error while getting the HN Feed');
    return null;
  }
};

export const putNewsInactiveAPI = async (id) => {
  const body = { active: false };
  try {
    let response = await httpRequest.put(`news/${id}`, body);

    if (response.status === 200) {
      return response.data;
    }
    throw Error(response);
  } catch (error) {
    console.log('There was an error while trying deactivate this news', error);
    return null;
  }
};
