import { format, subDays } from 'date-fns';
import trashIcon from '../../assets/trash.png';

const NewsElement = ({ data, put }) => {
  const { _id, title, story_title, url, story_url, created_at, author } = data;

  function formatDate() {
    let date_now = new Date();
    let yesterday = format(subDays(date_now, 1), 'dd');
    let today = format(date_now, 'dd');
    let news_date = new Date(created_at);
    let news_day = format(news_date, 'dd');

    if (today === news_day) {
      return format(news_date, 'h:mm aaaa');
    }
    if (yesterday === news_day) {
      return 'Yesterday';
    }
    return format(news_date, 'MMM d');
  }

  function getDisplay() {
    if (title || story_title) return '';
    return 'none';
  }
  return (
    <a
      className='container underscore fs news'
      style={{ display: getDisplay() }}
      href={story_url || url}
      target='_blank'
      rel='noreferrer'
    >
      <div className='col-7 mn row'>
        <p className='mls black'>{title || story_title}</p>
        <p className='mls grey'>- {author} -</p>
      </div>
      <div className='mn col-2 text-center black'>
        <p>{formatDate()}</p>
      </div>
      <div className='mn col-1 text-center' style={{ alignSelf: 'center' }}>
        <img
          src={trashIcon}
          width='20px'
          alt='erase button'
          onClick={(e) => {
            e.preventDefault();
            put(_id);
          }}
        ></img>
      </div>
    </a>
  );
};

export default NewsElement;
