import NewsElement from './news_element';

const NewsList = ({ data_array, put }) => {
  const list = data_array.map((data) => (
    <NewsElement data={data} key={data.objectID} put={put} />
  ));
  return (
    <div className='mls mrs'>
      {list.length === 0 ? <p>There are no more news to show</p> : list}
    </div>
  );
};

export default NewsList;
