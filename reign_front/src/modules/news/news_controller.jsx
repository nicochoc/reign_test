import { useEffect, useState } from 'react';
import { getNewsAPI, putNewsInactiveAPI } from '../../data/newsAPI';

const NewsController = () => {
  const [newsInfo, setNewsInfo] = useState([]);

  useEffect(() => {
    getNews();
  }, []);

  async function getNews() {
    let newsDocuments = await getNewsAPI();
    console.log(newsDocuments);
    if (newsDocuments == null) return;
    setNewsInfo(newsDocuments);
  }

  async function putNewsInactive(id) {
    let putNews = await putNewsInactiveAPI(id);
    if (putNews == null) return;
    await getNews();
  }

  return { newsInfo, getNews, putNewsInactive };
};

export default NewsController;
