
import { Fragment } from "react/cjs/react.production.min";
import Header from "./header";
import NewsController from "./news/news_controller";
import NewsList from "./news/news_list";

function App() {

  let {newsInfo, putNewsInactive} = NewsController();

  return (
    <Fragment>
      <Header />
      <NewsList data_array={newsInfo} put={putNewsInactive}/>
    </Fragment>

  );
}

export default App;
