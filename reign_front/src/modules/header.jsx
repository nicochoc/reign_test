const Header = () => {
  return (
    <div className='container header center'>
      <div className='col'>
        <h1 className='fxl'>HN Feed</h1>
        <h3 className='fl'>We &lt;3 hacker news!</h3>
      </div>
    </div>
  );
};

export default Header;
