# Reign Test for Full Stack

This Repo was made as a test for an interview with Reign.

## Cloning

Please clone this repo with both folders on it ( Reign_Front and Reign-back).   
run the following code in your terminal

```bash
git clone https://thisrepourl
```

## Docker-compose 

Make sure you have docker on your local machine 
                                                                                                         run the following code in your terminal
```
docker-compose up -d
```
This will create a MongoDB instance with the necessary API and Client

## Connecting to the Interface
On your local machine connect to http://localhost:3000

## For stoping and removing all local docker processes

run the following code in your terminal
```
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```

# Testing
For testing the server side app you should run the following commands
```
cd reign-back
npm i
npm run test:watch
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

