"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewsService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const news_schema_1 = require("./schemas/news.schema");
const schedule_1 = require("@nestjs/schedule");
const axios_1 = require("@nestjs/axios");
const rxjs_1 = require("rxjs");
let NewsService = class NewsService {
    constructor(model, httpService) {
        this.model = model;
        this.httpService = httpService;
        this.handleCron();
    }
    async handleCron() {
        let latest_news = await this.getLastNews();
        let lastest_created_at = latest_news === null || latest_news === void 0 ? void 0 : latest_news.created_at_i;
        let url = lastest_created_at
            ? `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&numericFilters=created_at_i>${lastest_created_at}`
            : 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
        let apiResponse = await this.getNewsFromAPI(url);
        apiResponse.forEach(async (news) => {
            await new this.model(Object.assign(Object.assign({}, news), { active: true })).save();
        });
    }
    async getNewsFromAPI(url) {
        let apiResponse = this.httpService.get(url);
        let response = await (0, rxjs_1.firstValueFrom)(apiResponse).then((response) => response.data.hits);
        return response;
    }
    async getLastNews() {
        let lastNews = await this.model.findOne().sort('-created_at_i').exec();
        return lastNews;
    }
    async findAll() {
        return await this.model.find({ active: true }).sort('-created_at_i').exec();
    }
    async findOne(id) {
        return await this.model.findById(id).exec();
    }
    async create(createNewsDto) {
        return await new this.model(Object.assign({}, createNewsDto)).save();
    }
    async update(id, updateNewsDto) {
        return await this.model
            .findByIdAndUpdate(id, updateNewsDto, { new: true })
            .exec();
    }
    async delete(id) {
        return await this.model.findByIdAndDelete(id).exec();
    }
};
__decorate([
    (0, schedule_1.Cron)('0 */1 * * *'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], NewsService.prototype, "handleCron", null);
NewsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(news_schema_1.News.name)),
    __metadata("design:paramtypes", [mongoose_2.Model,
        axios_1.HttpService])
], NewsService);
exports.NewsService = NewsService;
//# sourceMappingURL=news.service.js.map