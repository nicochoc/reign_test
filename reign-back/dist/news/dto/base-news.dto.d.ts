export declare class BaseNewsDto {
    created_at?: Date;
    description?: string;
    title?: string;
    author?: string;
    points?: any;
    story_text?: string;
    comment_text?: string;
    num_comments?: any;
    story_id?: number;
    story_title?: string;
    story_url?: string;
    parent_id?: number;
    created_at_i?: number;
    _tags?: any;
    objectID?: number;
    _highlightResult: any;
}
