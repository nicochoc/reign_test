import { BaseNewsDto } from './base-news.dto';
export declare class UpdateNewsDto extends BaseNewsDto {
    active: boolean;
}
