import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { NewsService } from './news.service';
export declare class NewsController {
    private readonly service;
    constructor(service: NewsService);
    index(): Promise<import("./schemas/news.schema").News[]>;
    find(id: string): Promise<import("./schemas/news.schema").News>;
    create(createNewsDto: CreateNewsDto): Promise<import("./schemas/news.schema").News>;
    update(id: string, updateNewsDto: UpdateNewsDto): Promise<import("./schemas/news.schema").News>;
    delete(id: string): Promise<import("./schemas/news.schema").News>;
}
