import { Document } from 'mongoose';
export declare type NewsDocument = News & Document;
interface Author {
    value: string;
    matchLevel: string;
    matchedWords: Array<any>;
}
interface CommentText {
    value: string;
    matchLevel: string;
    fullyHighlighted: boolean;
    matchedWords: Array<string>;
}
interface StoryTitle {
    value: string;
    matchLevel: string;
    matchedWords: Array<string>;
}
interface StoryUrl {
    value: string;
    matchLevel: string;
    matchedWords: Array<string>;
}
declare class highlightResult {
    author: Author;
    comment_text: CommentText;
    story_title: StoryTitle;
    story_url: StoryUrl;
}
export declare class News {
    created_at?: Date;
    description?: string;
    title?: string;
    points?: number;
    story_text?: string;
    comment_text?: string;
    num_comments?: number;
    story_id?: number;
    story_title?: string;
    story_url?: string;
    parent_id?: number;
    created_at_i?: number;
    _tags?: Array<any>;
    objectID?: number;
    _highlightResult?: highlightResult;
    active?: boolean;
}
export declare const NewsSchema: import("mongoose").Schema<Document<News, any, any>, import("mongoose").Model<Document<News, any, any>, any, any, any>, {}>;
export {};
