import { Model } from 'mongoose';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { News, NewsDocument } from './schemas/news.schema';
import { HttpService } from '@nestjs/axios';
export declare class NewsService {
    private readonly model;
    private readonly httpService;
    constructor(model: Model<NewsDocument>, httpService: HttpService);
    handleCron(): Promise<void>;
    getNewsFromAPI(url: string): Promise<News[]>;
    getLastNews(): Promise<News>;
    findAll(): Promise<News[]>;
    findOne(id: string): Promise<News>;
    create(createNewsDto: CreateNewsDto): Promise<News>;
    update(id: string, updateNewsDto: UpdateNewsDto): Promise<News>;
    delete(id: string): Promise<News>;
}
