import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { News, NewsDocument } from './schemas/news.schema';
import { Cron } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom, map } from 'rxjs';

@Injectable()
export class NewsService implements OnModuleInit {
  constructor(
    @InjectModel(News.name) private readonly model: Model<NewsDocument>,
    private readonly httpService: HttpService,
  ) {}

  onModuleInit() {
    this.handleCron();
  }

  @Cron('0 */1 * * *')
  async handleCron() {
    const latest_news = await this.getLastNews();
    const lastest_created_at = latest_news?.created_at_i;
    let url = lastest_created_at
      ? `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&numericFilters=created_at_i>${lastest_created_at}`
      : 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

    let apiResponse = await this.getNewsFromAPI(url);
    apiResponse.forEach(async (news) => {
      await new this.model({ ...news, active: true }).save();
    });
  }

  async getNewsFromAPI(url: string): Promise<News[]> {
    let apiResponse = this.httpService.get(url);
    let response = await firstValueFrom(apiResponse).then(
      (response) => response.data.hits,
    );
    return response;
  }

  async getLastNews(): Promise<News> {
    let lastNews = await this.model.findOne().sort('-created_at_i').exec();
    return lastNews;
  }

  async findAll(): Promise<News[]> {
    return await this.model.find({ active: true }).sort('-created_at_i').exec();
  }

  async findOne(id: string): Promise<News> {
    return await this.model.findById(id).exec();
  }

  async create(createNewsDto: CreateNewsDto): Promise<News> {
    return await new this.model({
      ...createNewsDto,
    }).save();
  }

  async update(id: string, updateNewsDto: UpdateNewsDto): Promise<News> {
    return await this.model
      .findByIdAndUpdate(id, updateNewsDto, { new: true })
      .exec();
  }

  async delete(id: string): Promise<News> {
    return await this.model.findByIdAndDelete(id).exec();
  }
}
