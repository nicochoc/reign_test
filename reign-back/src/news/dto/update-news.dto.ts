import { IsBoolean } from 'class-validator';
import { BaseNewsDto } from './base-news.dto';

export class UpdateNewsDto extends BaseNewsDto {
  @IsBoolean()
  active: boolean;
}
