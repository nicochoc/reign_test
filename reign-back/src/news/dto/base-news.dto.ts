import { IsDate, IsNumber, IsOptional, IsString } from 'class-validator';

export class BaseNewsDto {
  @IsOptional()
  @IsDate()
  created_at?: Date;
  @IsOptional()
  @IsString()
  description?: string;
  @IsOptional()
  @IsString()
  title?: string;
  @IsOptional()
  @IsString()
  url?: string;
  @IsOptional()
  @IsString()
  author?: string;
  @IsOptional()
  points?: any;
  @IsOptional()
  @IsString()
  story_text?: string;
  @IsOptional()
  @IsString()
  comment_text?: string;
  @IsOptional()
  num_comments?: any;
  @IsOptional()
  @IsNumber()
  story_id?: number;
  @IsOptional()
  @IsString()
  story_title?: string;
  @IsOptional()
  @IsString()
  story_url?: string;
  @IsOptional()
  @IsNumber()
  parent_id?: number;
  @IsOptional()
  @IsNumber()
  created_at_i?: number;
  @IsOptional()
  _tags?: any; // array
  @IsOptional()
  @IsNumber()
  objectID?: number;
  @IsOptional()
  _highlightResult: any; // object
}
