import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewsDocument = News & Document;

interface Author {
  value: string;
  matchLevel: string;
  matchedWords: Array<any>;
}

interface CommentText {
  value: string;
  matchLevel: string;
  fullyHighlighted: boolean;
  matchedWords: Array<string>;
}

interface StoryTitle {
  value: string;
  matchLevel: string;
  matchedWords: Array<string>;
}

interface StoryUrl {
  value: string;
  matchLevel: string;
  matchedWords: Array<string>;
}
@Schema()
class highlightResult {
  author: Author;
  comment_text: CommentText;
  story_title: StoryTitle;
  story_url: StoryUrl;
}
@Schema()
class Hola {
  value: number;
}

@Schema()
export class News {
  @Prop()
  created_at?: Date;

  @Prop()
  description?: string;

  @Prop()
  title?: string;

  @Prop()
  points?: number;

  @Prop()
  story_text?: string;

  @Prop()
  comment_text?: string;

  @Prop()
  num_comments?: number;

  @Prop()
  story_id?: number;

  @Prop()
  story_title?: string;

  @Prop()
  story_url?: string;

  @Prop()
  parent_id?: number;

  @Prop()
  created_at_i?: number;

  @Prop()
  _tags?: Array<any>;

  @Prop()
  objectID?: number;

  @Prop()
  _highlightResult?: highlightResult;

  @Prop()
  active?: boolean;
}

export const NewsSchema = SchemaFactory.createForClass(News);
