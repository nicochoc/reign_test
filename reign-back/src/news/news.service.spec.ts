import { HttpModule, HttpService } from '@nestjs/axios';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { NewsService } from './news.service';
import { News } from './schemas/news.schema';

const mockNews = {
  title: 'test',
  created_at_i: 1588888888,
  _highlightResult: {},
};

describe('NewsService', () => {
  let service: NewsService;
  let model: Model<News>;

  const newsArray = [
    {
      title: 'test',
      created_at_i: 1588888888,
    },
    {
      title: 'taz',
      created_at_i: 1588888888,
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        {
          provide: getModelToken('News'),
          useValue: {
            new: jest.fn().mockResolvedValue(mockNews),
            constructor: jest.fn().mockResolvedValue(mockNews),
            find: jest.fn(),
            create: jest.fn(),
            exec: jest.fn(),
          },
        },
      ],
      imports: [HttpModule],
    }).compile();

    service = module.get<NewsService>(NewsService);
    model = module.get<Model<News>>(getModelToken('News'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return all News', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      sort: (n) => {
        return {
          exec: jest.fn().mockResolvedValueOnce(newsArray),
        };
      },
    } as any);
    const news = await service.findAll();
    expect(news).toEqual(newsArray);
  });
});
